# Generated by Django 3.2 on 2021-05-05 05:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog_site', '0004_blogpost'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpost',
            name='thumbnail',
            field=models.ImageField(default='', upload_to='shop/img'),
        ),
    ]
