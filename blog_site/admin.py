from django.contrib import admin
from blog_site.models import Contact,Post, Product

# Register your models here.
# admin.site.register(Contact)

@admin.register(Contact)
class ContactAdmin(admin.ModelAdmin):
    list_display=['name','email','phone','desc','date']

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display=['sno','title','content','author','slug','timeStamp']

@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display=['product_name','price','desc','image']

 