from django.contrib import admin
from django.urls import path
from blog_site import views


urlpatterns = [
    path('', views.index,  name='home'),
    path("about", views.about,  name='about'),
    path("services", views.services,  name='services'),
    path("contact", views.contact,  name='contact'),
    path("welcome", views.welcome,  name='welcome'),
    path("signup", views.handleSignup,  name='handleSignup'),
    path('login', views.handeLogin, name="handleLogin"),
    path('logout', views.handelLogout, name="handleLogout"),
    path('blog', views.blogHome, name='blogHome'), 
    path('<str:slug>', views.blogPost, name='blogPost'), 
    path('search', views.search, name="search"),
    path('tracker', views.tracker,name="TrackingStatus"),
    path('productView', views.productView,name="productView"),
    path('checkout', views.checkout,name="checkout"),
]