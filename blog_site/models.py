from django.db import models


# Create your models here.

class Contact(models.Model):
    name = models.CharField(max_length=20)
    email = models.CharField(max_length=50)
    phone = models.CharField(max_length=20)
    desc = models.CharField(max_length=100)
    date = models.DateField()
    def __str__(self):
        return self.name


class Post(models.Model):
    sno=models.AutoField(primary_key=True)
    title=models.CharField(max_length=255)
    content=models.TextField()
    author=models.CharField(max_length=14)
    slug=models.CharField(max_length=130)
    timeStamp=models.DateTimeField(blank=True)
    def __str__(self):
        # return self.title + " by " + self.author
        return self.author

class Product(models.Model):
    product_name = models.CharField(max_length=20)
    price = models.IntegerField()
    desc = models.CharField(max_length=100)
    image = models.ImageField(upload_to='static/image', default="")
    
    def __str__(self):
        return self.product_name


