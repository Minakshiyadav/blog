from django.shortcuts import render, HttpResponse, redirect
from django.contrib.auth.models import User
from django.contrib.auth  import authenticate,  login, logout
from datetime import datetime
from blog_site.models import Contact, Post, Product
from django.contrib import messages # for messeges Alert


# Create your views here.

def index(request):
    productss = Product.objects.all() 
    return render(request, 'index.html', {'productss': productss})
    # return HttpResponse('This is Home page')

def tracker(request):
    return HttpResponse("This is tracker")

def productView(request):
    return HttpResponse("We are at product view")

def checkout(request):
    return HttpResponse("We are at checkout")

def blogHome(request):
    allPosts = Post.objects.all() 
    return render(request, 'blogHome.html', {'allPosts': allPosts})
    
def blogPost(request, slug): 
    post=Post.objects.filter(slug=slug).first() # if give id then .filter(post_id = id)[0] 
    return render(request, "blogPost.html", {"post":post})

def about(request):
    return render(request, 'about.html')

def services(request):
    return render(request, 'services.html')
# Contact Page
def contact(request):
    if request.method == "POST":
        name = request.POST.get('name')
        email = request.POST.get('email')
        phone = request.POST.get('phone')
        desc = request.POST.get('desc') 
        contact = Contact(name= name, email= email, phone= phone, desc= desc, date= datetime.today())   
        contact.save()
        messages.success(request, 'Messages has been sent!!!') # for messeges alert
    return render(request, 'contact.html')

def search(request):
    query=request.GET['query']
    if len(query)>78:
        allPosts=Post.objects.none()
    else:
        allPostsTitle= Post.objects.filter(title__icontains=query)
        allPostsAuthor= Post.objects.filter(author__icontains=query)
        allPostsContent =Post.objects.filter(content__icontains=query)
        allPosts=  allPostsTitle.union(allPostsContent, allPostsAuthor)
    if allPosts.count()==0:
        messages.warning(request, "No search results found. Please refine your query.")
    params={'allPosts': allPosts, 'query': query}
    return render(request, 'search.html', params)

def welcome(request):
    return render(request, 'welcome.html')


def handleSignup(request):
    if request.method == "POST":
        username = request.POST['username']
        fname = request.POST['fname']
        lname = request.POST['lname']
        email = request.POST['email']
        passward1 = request.POST['pass1']
        passward2 = request.POST['pass2']

        # check for errorneous input
        if len(username)<10:
            messages.error(request, " Your user name must be under 10 characters")
            return redirect('/')

        if not username.isalnum():
            messages.error(request, " User name should only contain letters and numbers")
            return redirect('/')
        if User.objects.filter(username=username).exists():
            messages.warning(request, "User name already exist")
            return redirect('/')

        if (passward1!= passward2):
             messages.error(request, " Passwords do not match")
             return redirect('/')
        
        # Create the user

        myuser= User.objects.create_user(username,email,passward1)
        myuser.first_name= fname
        myuser.last_name= lname
        myuser.save()
        messages.success(request, "You Account has been Successfully Crated")
        return redirect ('/')
    
    else:
         return HttpResponse("404 Error")

def handeLogin(request):
    if request.method=="POST":
        # Get the post parameters
        loginusername=request.POST['loginusername']
        loginpassword=request.POST['loginpassword']

        user=authenticate(username= loginusername, password= loginpassword)
        if user is not None:
            login(request, user)
            messages.success(request, "Successfully Logged In")
            return redirect("/")
        else:
            messages.error(request, "Invalid credentials! Please try again")
            return redirect("/")
    return HttpResponse("404- Not found")

def handelLogout(request):
    logout(request)
    messages.success(request, "Successfully logged out")
    return redirect("/")

