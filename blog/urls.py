
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('blog_site.urls'))
]

# Beloww code use only for change the name of admin heading  text
admin.site.site_header = "BS Admin"
admin.site.site_title = "BS Admin Portal"
admin.site.index_title = "Welcome to BS Researcher Portal"